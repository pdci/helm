#!/usr/bin/env bash

set -ex
PDCI_APP=sisbio
PDCI_HELM_VALUE_TEMPLATE=values.pdci.yaml
PDCI_HELM_VALUE=values.deploy.yaml
#PATH_KUBECTL=/var/jenkins_home
PATH_KUBECTL=${WORKSPACE:-/usr/bin/}
PATH_KUBEVAL=${WORKSPACE:-/usr/local/bin/}
PATH_MANIFESTO_DEPLOY=${WORKSPACE}/k8s/deploy
PARAM_KUBECONFIG="--kubeconfig=~/.kube/config"

rm -rf value.yaml

###instalacao do kubectl
#if [ ! -f  "${PATH_KUBEVAL}/kubectl" ]; then
#  curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/linux/amd64/kubectl
#  chmod 777 "${PATH_KUBECTL}/kubectl"
#  chmod +x  "${PATH_KUBECTL}/kubectl"
#fi
#
###instalacao do kubeval
###Verificar sintaxe e boas prativas.
#if [ ! -f  "${PATH_KUBEVAL}/kubeval" ]; then
#  wget https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz
#  tar xf kubeval-linux-amd64.tar.gz
#  #sudo cp kubeval /usr/local/bin
#  chmod 777 "${PATH_KUBEVAL}/kubeval"
#  chmod +x  "${PATH_KUBEVAL}/kubeval"
#fi


export PDCI_FILE_ENV_DOCKER=../../.env.docker.dev


if [ ! -f  ${PDCI_FILE_ENV_DOCKER} ]; then

  echo ""
  echo "===================================== ERRO =================================================="
  echo ""
  echo "Arquivo de variáveis de ambiente ( ${PDCI_FILE_ENV_DOCKER} ) do docker da aplicação. "
  echo ""
  echo "============================================================================================="
  exit 1
fi

cat  ${PDCI_FILE_ENV_DOCKER}

v_p=${PDCI_FILE_ENV_DOCKER}
if [ -f "${v_p}" ] ; then
  echo "Setando PDCI variables (__pdci_set_) no arquivo ${v_p}."
  v_pdci=''
  for v_i in `printenv |  sed 's/\x0D$//'  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '__pdci_set_' | cut -d'=' -f1 | sed 's/__pdci_set_/${__pdci_set_/g' | awk '{print $0 "}"'}`
  do
     echo "Discovery PDCI Variable  ${v_i} "
     if [ "${v_pdci}" != "" ]; then
     v_pdci="${v_pdci},"
     fi
     v_pdci="${v_pdci}  ${v_i} "
  done

  echo "Replace PDCI variables."

  cp -f -v  ${v_p} ${v_p}.bkp
  #echo "v_envsubst_add"
  v_envsubst="envsubst ' ${v_pdci}' < \"${v_p}.bkp\" > \"${v_p}\""
  echo "v_envsubst=> ${v_envsubst}"
  eval ${v_envsubst}

else
  echo "Arquivo ENV.DOCKER não existe. ${v_p}"
fi



source ${PDCI_FILE_ENV_DOCKER}
export $(cut -d= -f1 ${PDCI_FILE_ENV_DOCKER})

export PDCI_GIT_BRANCH=$(echo ${__PDCI_RELEASE:-develop} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")

echo ""
echo "PDCI_GIT_BRANCH => ${PDCI_GIT_BRANCH}"
echo ""

echo ""
echo "============================================================================================="
echo ""
echo "    Exportando variaveis do arquivo  ${PDCI_FILE_ENV_DOCKER} "
echo ""
echo "============================================================================================="

rm -rf ${PDCI_APP}/${PDCI_HELM_VALUE} && \
rm -rf ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp && \
cp -v ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE} ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp && \
envsubst  < "${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp" >> "${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE}"

#cat ${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE}
cat ${PDCI_APP}/${PDCI_HELM_VALUE}

if [ -f ${PDCI_APP}/Chart.lock ]; then

  echo ""
  echo "============================================================================================="
  echo ""
  echo "    Atualizando dependencias do chart com o Helm "
  echo ""
  echo "============================================================================================="

  helm dependency update ${PDCI_APP}

  ls -la ${PDCI_APP}

else

  echo ""
  echo "============================================================================================="
  echo ""
  echo "    Construindo dependencias do chart com o Helm "
  echo ""
  echo "============================================================================================="

  helm dependency build ${PDCI_APP}

   ls -la ${PDCI_APP}

fi


echo ""
echo "============================================================================================="
echo ""
echo "    Manifesto do deploy com o Helm "
echo ""
echo "============================================================================================="

helm install ${PDCI_APP} ${PDCI_APP} --values ${PDCI_APP}/${PDCI_HELM_VALUE} --dry-run --debug


echo ""
echo "============================================================================================="
echo ""
echo "    Realizando deploy com o chart do Helm "
echo ""
echo "============================================================================================="

if [ ${PDCI_HELM_UNINSTALL} ]; then

helm uninstall ${PDCI_APP}
sleep 60

fi


#helm upgrade --install ${PDCI_APP} ${PDCI_APP} --values ${PDCI_APP}/${PDCI_HELM_VALUE} --wait
helm upgrade --install ${PDCI_APP}pdci ${PDCI_APP} --values ${PDCI_APP}/${PDCI_HELM_VALUE} --wait

exit 0

echo ""
echo "*****************************************"
echo "events"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get events
echo ""
echo "*****************************************"
echo "pv"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get pv
echo ""
echo "*****************************************"
echo "pvc"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get pvc
echo ""
echo "*****************************************"
echo "service"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get service
echo ""
echo "*****************************************"
echo "deployment"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get deployment
echo ""
echo "*****************************************"
echo "Describe deployment"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} describe deployment
echo ""
echo "*****************************************"
echo "pods"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get pods
echo ""
echo "*****************************************"
echo "Describe pods"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} describe pods
echo ""
echo "*****************************************"
echo "endpoints"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get endpoints
echo ""
echo "*****************************************"
echo "ingress"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get ingress
echo ""
echo "*****************************************"
echo "services"
echo ""
${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} get svc
echo ""
echo "*****************************************"
echo "nodes"
echo ""
${PATH_KUBECTL}/kubectl  --kubeconfig=${KUBECONFIG}  -l name=prod  top  node
echo ""
echo "*****************************************"
echo "Describe nodes"
echo ""
${PATH_KUBECTL}/kubectl  --kubeconfig=${KUBECONFIG}  -l name=prod  describe  node


#${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} logs -f  --tail=-1

#${PATH_KUBECTL}/kubectl -l app=${PDCI_APP} --namespace=devops  --kubeconfig=${KUBECONFIG} exec --stdin --tty {pod} -- /bin/bash

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | sed -E 's/(.*)=.*/\1/' | xargs)