#!/usr/bin/env bash

#for FILE in $(find inbox/ -type f \( -iname '*.xlsx' -o -iname '*.xls' -o -iname '*.ods' \) )
#for FILE in $(find inbox/MUKIRA_SISMONITORA_e_SINTAX.xlsx )
for FILE in $(find "Chart.yaml" )
	do
	    echo "Chart encontrado ${FILE}"
done
mkdir -p ./packages || true
helm package sisicmbio -d ./packages
helm package pdci -d ./packages
helm repo index --merge ./packages/index.yaml --url https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME} ./packages
ls -la ./packages
cat ./packages/index.yaml
git add .
git commit -m "Adicionado chart "
git push origin "add-helm-chart"