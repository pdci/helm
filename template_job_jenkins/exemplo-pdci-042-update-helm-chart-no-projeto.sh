#!/bin/bash
set -ex

export  PDCI_NAME_GROUP=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $4}')
export  PDCI_NOME_PROJETO=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )
export  PDCI_NOME_PROJETO_MAINAPP=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}' | sed -e "s/.git//g" | sed -e "s/app_//g" )

export PDCI_UPPERCASE_NOME_PROJETO_MAINAPP="${PDCI_NOME_PROJETO_MAINAPP^^}";


pwd
cd  nome_projeto_mainapp

sed -i "s|name: sisicmbio|name: ${PDCI_NOME_PROJETO_MAINAPP}|g" ./Chart.yaml
VERSION_CHART=$(/var/jenkins_home/yq e '.version' Chart.yaml)
echo ""
echo "VERSION_CHART => ${VERSION_CHART}"
echo ""

mv ./values.pdci.yaml ./values.template.yaml
sed -i "s/^/  /" ./values.template.yaml
#sed -i "4s/^/app:/" ./values.pdci.yaml

#cat ./values.pdci.yaml

echo "
dependencies:" >> ./Chart.yaml
#/var/jenkins_home/yq e '.services' ../../../docker-compose.yml
#/var/jenkins_home/yq e -i '.app.image.repository = "registry.gitlab.com/sisicmbio/app_sisbio/app"' Chart.yaml

for identityMapping in $(/var/jenkins_home/yq e '.services | keys' ../../../docker-compose.yml); do
     echo "identityMapping => ${identityMapping}"
     if [[ "${identityMapping}" != "-" ]]; then
       echo ""
       IMAGE=$(eval "/var/jenkins_home/yq e '.services.${identityMapping}.image' ../../../docker-compose.yml | sed \"s|:[\$]{PDCI_GIT_BRANCH}||g\" " )
       echo "IMAGE => ${IMAGE}"
       echo ""
       if [[ "${identityMapping}" = "${PDCI_NOME_PROJETO_MAINAPP}" ]]; then
         echo ""
         echo "servico principal app"
         echo ""
echo "  - name: sisicmbio
    version: \"${VERSION_CHART}\"
    repository: https://pdci.gitlab.io/helm
    alias: app" >> ./Chart.yaml
       cat ./values.template.yaml | sed "4s/^/app:/"  >> ./values.pdci.yaml
       eval "/var/jenkins_home/yq e -i '.app.image.repository = \"${IMAGE}\"' ./values.pdci.yaml"
       eval "/var/jenkins_home/yq e -i '.appVersion = \"\${PDCI_GIT_BRANCH}\"' ./Chart.yaml"
       else
         NOME_SERVICO=${identityMapping//${PDCI_NOME_PROJETO_MAINAPP}/}
         NOME_SERVICO=${NOME_SERVICO//_/-}
         NOME_SERVICO="${NOME_SERVICO%\-}"
         NOME_SERVICO="${NOME_SERVICO#\-}"
         echo "  - name: sisicmbio
    version: \"${VERSION_CHART}\"
    repository: https://pdci.gitlab.io/helm
    alias: ${NOME_SERVICO}" >> ./Chart.yaml
         cat ./values.template.yaml | sed "4s/^/${NOME_SERVICO}:/"  >> ./values.pdci.yaml
         eval "/var/jenkins_home/yq e -i '.${NOME_SERVICO}.image.repository = \"${IMAGE}\"' ./values.pdci.yaml"
         eval "/var/jenkins_home/yq e -i '.appVersion = \"\${PDCI_GIT_BRANCH}\"' ./Chart.yaml"

       fi
     fi
done

MYVARS='$PDCI_UPPERCASE_NOME_PROJETO_MAINAPP:$PDCI_NOME_PROJETO_MAINAPP:$PDCI_NOME_PROJETO:$PDCI_NAME_GROUP'

for f in $(find  -name "*" -type f );
do
    echo "Processando arquivo do template do helm ${f}"
    rm -rf "${f}.bkp"
    mv "${f}" "${f}.bkp"
    envsubst "$MYVARS" < "${f}.bkp" > "${f}"
    rm -rf "${f}.bkp"
done

mkdir -p  ../../../k8s/helm/${PDCI_NOME_PROJETO_MAINAPP}

cp -rvf ./* ./../../../k8s/helm/${PDCI_NOME_PROJETO_MAINAPP}/

declare -a arr_env=(".env.docker.dev"
".env.docker.tcti"
".env.docker.hmg"
".env.docker.prd"
".env.docker.trn")

for env in "${arr_env[@]}"; do #for i in `ls exemplo-db-pdci-*{db_prd_name}*.xml`




    # if [[ !  -f  ${PDCI_FILE_ENV_DOCKER} ]]; then
    #   export PDCI_FILE_ENV_DOCKER=../../.env.docker.tcti
    # fi

    if [[  -f  ${WORKSPACE}/${env} ]]; then
        ENVK8S=${env//docker./docker.k8s.}
        cp -rf ${WORKSPACE}/$env ${WORKSPACE}/${ENVK8S}
        export PDCI_FILE_ENV_DOCKER="${WORKSPACE}/${ENVK8S}"

        echo ""
        echo "========================================================================================"
        echo ""
        echo "Projeto ${env}"
        echo "env k8s ${ENVK8S}"
        echo ""
        echo "========================================================================================"
        echo ""
        echo "Etapa 1 remover variaveis existentes em ${PDCI_FILE_ENV_DOCKER} ."
        echo ""
        sed -i '/PDCI_DEPLOY_DEVELOPMENT/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/PDCI_PERSISTENTVOLUME_NFS_SERVER_IP/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/http_proxy/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/https_proxy/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/no_proxy/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/HTTP_PROXY/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/HTTPS_PROXY/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/NO_PROXY/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/PDCI_BD_PASSWORD/d' ${PDCI_FILE_ENV_DOCKER}
        sed -i '/PDCI_DB_PASSWORD/d' ${PDCI_FILE_ENV_DOCKER}

        echo ""
        echo "Etapa 2 adicionar variávies existentes em ${PDCI_FILE_ENV_DOCKER} ."
        echo ""
        case ${env} in
        *".env.docker.dev"*)
            echo ""
            echo " Setando variaveis para ${env} ."
            echo "
PDCI_DEPLOY_DEVELOPMENT='true'
PDCI_BD_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
PDCI_DB_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
" >> ${PDCI_FILE_ENV_DOCKER}
            ;;
        *".env.docker.tcti"*)
            echo ""
            echo " Setando variaveis para ${env} ."
            echo ""
            echo "
PDCI_DEPLOY_DEVELOPMENT='false'
PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH='/NFS_ICMBIO_KUBERNETES'
PDCI_PERSISTENTVOLUME_NFS_SERVER_IP='10.10.4.250'
http_proxy=http://proxy.icmbio.gov.br:8080
https_proxy=http://proxy.icmbio.gov.br:8080
no_proxy=127.0.0.1,dbtcti01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
HTTP_PROXY=http://proxy.icmbio.gov.br:8080
HTTPS_PROXY=http://proxy.icmbio.gov.br:8080
NO_PROXY=127.0.0.1,dbtcti01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
PDCI_BD_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
PDCI_DB_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
" >> ${PDCI_FILE_ENV_DOCKER}
            ;;
         *'.env.docker.hmg'*)
            echo ""
            echo " Setando variaveis para ${env} ."
            echo "
PDCI_DEPLOY_DEVELOPMENT='false'
PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH='/NFS_ICMBIO_KUBERNETES'
PDCI_PERSISTENTVOLUME_NFS_SERVER_IP='10.10.4.250'
http_proxy=http://proxy.icmbio.gov.br:8080
https_proxy=http://proxy.icmbio.gov.br:8080
no_proxy=127.0.0.1,dbhmg01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
HTTP_PROXY=http://proxy.icmbio.gov.br:8080
HTTPS_PROXY=http://proxy.icmbio.gov.br:8080
NO_PROXY=127.0.0.1,dbhmg01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
PDCI_BD_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
PDCI_DB_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
 " >> ${PDCI_FILE_ENV_DOCKER}
            ;;
         *".env.docker.prd"*)
            echo ""
            echo " Setando variaveis para ${env} ."
            echo ""
            echo "
PDCI_DEPLOY_DEVELOPMENT='false'
PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH='/data-docker-cluster/devops'
PDCI_PERSISTENTVOLUME_NFS_SERVER_IP='10.197.93.69'
http_proxy=http://proxy.icmbio.gov.br:8080
https_proxy=http://proxy.icmbio.gov.br:8080
no_proxy=10.197.32.211,127.0.0.1,dbprd01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
HTTP_PROXY=http://proxy.icmbio.gov.br:8080
HTTPS_PROXY=http://proxy.icmbio.gov.br:8080
NO_PROXY=10.197.32.211,127.0.0.1,dbprd01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
PDCI_BD_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
PDCI_DB_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
 " >> ${PDCI_FILE_ENV_DOCKER}
            ;;
         *".env.docker.trn"*)
            echo ""
            echo " Setando variaveis para ${env} ."
            echo ""
            echo "
PDCI_DEPLOY_DEVELOPMENT='false'
PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH='/NFS_ICMBIO_KUBERNETES'
PDCI_PERSISTENTVOLUME_NFS_SERVER_IP='10.10.4.250'
http_proxy=http://proxy.icmbio.gov.br:8080
https_proxy=http://proxy.icmbio.gov.br:8080
no_proxy=127.0.0.1,dbprd01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
HTTP_PROXY=http://proxy.icmbio.gov.br:8080
HTTPS_PROXY=http://proxy.icmbio.gov.br:8080
NO_PROXY=127.0.0.1,dbprd01.icmbio.gov.br,*.sisicmbio.icmbio.gov.br,*.icmbio.gov.br
PDCI_BD_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
PDCI_DB_PASSWORD=\${__pdci_set_db_password_pass_${PDCI_NOME_PROJETO_MAINAPP}}
 " >> ${PDCI_FILE_ENV_DOCKER}
            ;;
         *)
         echo ""
         ;;
    esac


    fi

done

rm -rf ${WORKSPACE}/k8s/helm/${PDCI_NOME_PROJETO_MAINAPP}/values.template.yaml